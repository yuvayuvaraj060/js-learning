import Student, {
  printStudentDetails,
  schoolName,
  printStudentName,
} from "./modules.mjs";

const student = new Student("Hari", 18, "12th");

printStudentDetails(student);

printStudentName(student);

console.log("Student School name: ", schoolName);
