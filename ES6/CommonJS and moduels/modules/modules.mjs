export default class Student {
  constructor(name, age, std) {
    this.name = name;
    this.age = age;
    this.std = std;
  }
}

export const printStudentDetails = (student) => {
  console.log(`${student.name}, ${student.age}, ${student.std}`);
};

const printStudentName = ({ name }) => {
  console.log("Student Name: ", name);
};

const schoolName = "DHSS";
export { printStudentName, schoolName };
