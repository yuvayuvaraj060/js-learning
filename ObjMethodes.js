// call method

const obj1 = {
  firstName: "Yuvaraj",
  lastName: "S",
};

const fullName = function (homePlace) {
  console.log(`${this.firstName} ${this.lastName}, ${homePlace}`);
};
fullName.call(obj1, "CNP");

const fullName2 = function (homePlace, pinCode) {
  console.log(`${this.firstName} ${this.lastName}, ${homePlace}, ${pinCode}`);
};
fullName2.call(obj1, "CNP", 624301); // call method

// apply method
fullName2.apply(obj1, ["CNP", 624301]); // apply method

const printDetails = fullName2.bind(obj1, "CNP", 624301); // bin method can use for call later

console.log(printDetails);
printDetails();
