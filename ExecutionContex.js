// Execution Context

const a = 2; // variable Declaration

// function declaration
const square = (n) => {
  return n * n;
};

//function invocation
const square1 = square(a); // pass by value //4
const square2 = square(4); // pass by value // 16

console.log("square1:", square1); //4
console.log("square2:", square2); //16


/*
compiling face
-------------------------------------------------       Call Stack
Memory                 ||     Code Block                Global()
-------------------------------------------------
a: undefined            ||      nothing
square: {...}           ||
                        ||
square1: location of    ||
the square function     ||
                        ||
square2: location of    ||
the square function     ||




Execution Face

-------------------------------------------------
Memory                 ||     Code Block
-------------------------------------------------
a: 2                    ||      square:
square: {...}           ||    Mem   |   code
                        ||
square1: location of    ||
the square function     ||
                        ||
square2: location of    ||
the square function     ||



*/
