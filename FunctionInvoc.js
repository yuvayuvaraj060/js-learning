// Function invocation

function notInvoc(x, y) {
  return x * y;
}

function functionInvocation(x, y) {
  return x * y;
}

const Obj = {
  firstName: "Yuavaraj Pandiyan",
  lastName: "S",
  AddName: function () {
    return `${this.firstName} ${this.lastName}`;
  },
};

console.log(notInvoc); // not invoked when we put the parentheses after the function name then only the function will call
console.log(functionInvocation(2, 4)); // func called

console.log(Obj.AddName()); // function invocation inside the object
