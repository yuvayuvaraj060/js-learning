// pass value
const a = 10;

function print(a) {
  console.log(a);
}

print(a);

// pass by ref

const Obj = {
  a: 10,
  b: 20,
  c: 30,
};

function ObjPassByRef(obj) {
  console.log(obj);
  obj.a = 100;
  console.log(obj);
}
ObjPassByRef(Obj);
console.log(Obj); // it will change the original obj becurse its pass by ref

const arr = [1, 2, 3, 4];

function arrPassByRef(a) {
  console.log(a);
  a[0] = 10;
  console.log(a);
}
arrPassByRef(arr);
console.log(arr); // it will change the original arr becurse its pass by ref
