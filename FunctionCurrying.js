// function curring

// using normal function with bind method

function multiple(x, y) {
  return x * y;
}

console.log(multiple(2, 3)); // normal function call

const multipleBy2 = multiple.bind(this, 2);
console.log(multipleBy2(5));

// function currying with closures

// closure function

const closure = (x) => {
  return (y) => {
    return x * y;
  };
};

const multipleBy10 = closure(10);
console.log(multipleBy10(20));

const multipleBy100 = closure(100);
console.log(multipleBy100(10));
