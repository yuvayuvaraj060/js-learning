// Async && await
const fetch = require("node-fetch");

const API = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await response.json();
  console.log(data);
};

API();
