// promises
const fetch = require("node-fetch");

// Promises syntax new Promises((resolve, reject) => {...})

const promises1 = new Promise((resolve, reject) => {
  if (5 + 5 == 10) {
    resolve("successfully Done");
  } else {
    reject("Failed");
  }
});

promises1
  .then((message) => {
    console.log(message);
  })
  .catch(() => {
    console.log(message);
  });

//fetch promises please run the fetch other wise install npm i node-fetch --save

const jsonFetch = fetch("https://jsonplaceholder.typicode.com/posts").then(
  (res) => res.json()
);
jsonFetch.then((data) => {
  console.log(data);
});
// resolve all promise at once
const promises2 = new Promise((resolve, reject) => {
  if (5 + 5 == 10) {
    resolve("successfully Done!");
  } else {
    reject("Failed");
  }
});

const promises3 = new Promise((resolve, reject) => {
  if (5 + 5 == 10) {
    resolve("successfully Done");
  } else {
    reject("failed");
  }
});

Promise.all([promises1, promises3, promises2])
  .then((message) => {
    console.log(message);
  })
  .catch((message) => {
    console.log(message);
  });
