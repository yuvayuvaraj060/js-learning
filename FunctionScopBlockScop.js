//  Block Scop
//  let and const are block scoped it no mean this let and const are block
{
  // valid block in the javascript
}

var a = 10; // global scope

{
  let a = 100; // block scoped and shadowing also

  console.log("Coming from block", a);
}
console.log("Coming from global", a);

var c = 10; // global scope

{
  const c = 100; // block scoped and shadowing also

  console.log("Coming from block", c);
}
console.log("Coming from global", c);

let b = 1;
{
  let b = 10;
  console.log("Coming from block", b);
}
console.log("Coming from global", a);

//  using the block scope the scope chain works
var d = 70;
{
  let d = 71;
  console.log("level 1", d);
  {
    let d = 72;
    console.log("level 2", d);
    {
      let d = 73;
      console.log("level 3", d);
      {
        let d = 74;
        console.log("level 4", d);
      }
    }
  }
}
console.log("global d", d);
/* ---------------------------------------------------------------------------------------- */

//  function scoped


function funcScop() {
  var fn = 100000;
  console.log(fn);
}
funcScop(); // after executing this function the execution context get vanished
try {
  console.log(fn);
} catch {
  console.log("fn variable not declared");
} // throws a error variable fn not declared

let ifAlsoFunction = "globally present";
function bb() {
  var ifAlsoFunction = "Only inside this function it present";
  console.log(ifAlsoFunction);
}
console.log(ifAlsoFunction);
