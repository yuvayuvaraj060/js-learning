// coercion

// consider as a false in js
/*
false
0
""
null
undefined
 */

//  empty array == 0
console.log("empty array: [] === 0", [] == 0);

console.log(5 + "5"); // number converted as a string

console.log(10 - "5"); // string converted as a number

//  == is do coercion and compare === is do not coercion and compare

console.log(5 == "5"); // true

console.log(5 === "5"); // false

// boolean consider as 0 or 1 true = 1, false = 0

console.log(true + 1); // 2
console.log(true - 1); // 0
console.log(false - 1); // -1
console.log(false + 1); // 1

if ([]) {
  console.log("im Inside");
}

if ("") {
  console.log("im Inside");
}

if (" ") {
  console.log("im Inside");
}

if (null) {
  console.log("im Inside");
}


if (undefined) {
    console.log("im Inside");
  }
