// Hoisting

console.log(a); // Hoisting using a variable or function before  declaration
console.log(func); // function declared by the function key word "This not a function invocation im trying to print the function"
func(); // This is a function invocation now the exact function will executed

console.log(func2); // arrow function or variable function
var a = 10;
function func() {
  console.log("func executed");
}

var func2 = () => {
  console.log(
    "it will not work only shows undefined becurse this function treated as variable declaration while MEM allocation"
  );
};

// it will not work only shows undefined becurse this function treated as variable declaration while MEM allocation

console.log(change, noteChange);

var change = 10;
var noteChange;

// const n; // syntax error
// let nn; // cannot accesses nn