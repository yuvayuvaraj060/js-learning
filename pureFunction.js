//  pure function as known as functional programing

/*  pure function not using any variables,
 objects and arrays in the outer scope of the function 
 it only taking data in parameters and returns new obj or array or value
 should not modified any thing existed and,
no AJAX calls allowed inside function*/

function pure(a, b) {
  return [...a, ...b, 10, 30];
}
const outPut1 = pure([1, 2, 3, 4, 5], [6, 7, 8]);
console.log(outPut1);

function Obj(a, b) {
  return {
    ...a,
    ...b,
    c: 123,
    D: "dsfiuhdsfi",
    e: {
      name: "yuvi",
      id: 07,
      ...a,
    },
  };
}

const outPut2 = Obj({ a: 1, b: 2 }, { c: 3, d: 4 });
console.log(outPut2);
