// Lexical Environment

// EX - 1
// hear js create lexical environment called a inside the env all the function variables has to store
function a() {
  console.log("Global Lexical  Scope", val); 
}
var val = 10; //Global Variable
// a();

/* -------------------------------------------------------------- */

// EX - 2
function parent() {
  var value = 5; // parent lexical scope
  child();
  function child() {
    console.log(value);
  }
}
// parent(); 

/* -------------------------------------------------------------- */

// EX - 3 also example for scope chining
function parent() {
  var value = 15; // parent lexical scope
  child();
  function child() {
    console.log("Child lexical scope", value); // 15
    child1();
    function child1() {
      value = 20; // reassigned value variable as 20
      child2();
      function child2() {
        console.log("Child2 lexical scope reassigned as 20", value); // 20
      }
    }
  }
}
parent();
