// variable environment

var a = 1; // global variable

b(); // also for also Hoisting example
c(); // also for also Hoisting example
console.log("from global variable env", a);
function b() {
  var a = 10;
  console.log("from b variable env", a);
}

function c() {
  var a = 100;
  console.log("from c variable env", a);
}
/*-------------- this code will throw the error */

// function c() {
//   var aa = 100;
//   console.log("from c variable env", aa);
// }
// c();
// console.log(aa);

const bbn = 0;
function c() {
  const bbn = 100;
  console.log("from c variable env", bbn);
}
c();
console.log("from global variable env", a);
