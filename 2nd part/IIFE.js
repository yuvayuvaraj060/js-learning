// normal function declaration

function func() {
  /* */
}
func();

// Immediately invoked function execution (IIFE)
(function () {
  console.log("print");
})();

const arr = [1, 2, 3, 4];

(function (arr) {
  arr.map((item) => console.log(item));
})(arr);
