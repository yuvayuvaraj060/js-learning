// prototype
var Obj = {
  name: "yuvi",
  class: "10th",
  printName() {
    console.log(this.name);
  },
};
var Ob = {
  name: "Hari",
};
var Obj2 = Object.setPrototypeOf(Ob, Obj); // Making a Ob object a parent scope or parent lexical scope the Obj is a child scope (lives in a Ob prototype)

// Object.create
const phone = {
  OSVersion: "Android 11",
  Battery: "5000mAH",
  printBatterySize() {
    console.log(this.Battery);
  },
};

const NewPhone = Object.create(phone);
// adding values in new object
NewPhone.name = "9 Pro";

// adding methods in array prototype
Array.prototype.myBind = function () {
  console.log("dfhdsg");
};
var arr = [1, 2, 3, 4, 5, 5, 6];
