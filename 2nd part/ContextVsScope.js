// context basically means object scope

// context refers the object belonging to the method
// scope is refers to a global space or function space other then the object

// context

// this keyword refers method belonging to the Obj
const Obj = {
  location: "India",
  printLocation() {
    console.log(this.location);
  },
};

Obj.printLocation();

const Obj1 = {
  location: "USA",
  printLocation() {
    console.log(this.location);
  },
};

Obj.printLocation.apply(Obj1);

// scope
var a = 10; // global scope a variable

function func() {
  var aa = 100; // function scoped
}
func();
try {
  console.log(aa);
} catch {
  console.log('"aa" not defined');
}