// in node window is referred as global or globalThis
// console.log(global);
// console.log(globalThis); // some methods node gives us
// console.log(this); // {}

// use full this inside the object
const obj = {
  arr1: [1, 2, 3, 4, 5, 6],
  sumArray() {
    return this.arr1.reduce((sum, num) => sum + num);
  },
  printThis() {
    console.log(this);
  },
};

console.log(obj.sumArray());
obj.printThis();

// create new object using function
function add(x, y) {
  this.x = x;
  this.y = y;
  this.add = () => {
    return this.x + this.y;
  };
}

const newAdd = new add(1, 3);
console.log(newAdd);
console.log(newAdd.add());

class car {
  constructor(brand, color, bookYear, life, milage) {
    this.brand = brand;
    this.color = color;
    this.bookYear = bookYear;
    this.life = life;
    this.milage = milage;
  }

  printColor() {
    return this.color;
  }
}

const newCar = new car("audi", "red", 2021, 16, 5);
console.log(newCar);
console.log(newCar.printColor());
