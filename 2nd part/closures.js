// closures

const a = () => {
  var b = 10;
  return () => {
    console.log(b);
  };
};

a()();



const timeOut = () => {
  for (var i = 0; i < 5; i++) {
    function run(x) {
      setTimeout(() => console.log(x), 1000);
    }
    run(i);
  }
};
timeOut();