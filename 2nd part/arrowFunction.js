// arrow function

// traditional function

function name(name) {
  return name;
}

// arrow function

const arrowFunction = () => console.log("arrow function");

const printName = (name) => console.log(name);

const addNum = (x, y) => x + y;

const objChangeDate = (arr) => {
  return arr.map((item) => {
    item.date = new Date();
    return { ...item };
  });
};

const changed = objChangeDate([
  {
    name: "yuvi",
    date: "21.12.2005",
  },
  {
    name: "yuvi",
    date: "21.12.2005",
  },
]);

console.log(changed);
