// first class citizens

/*  First class citizens are a functions in JS
// 1
can pass a function as a parameter

// 2
can store a function in variables

// 3
can return a function inside a function
*/

// 1 can pass a function as a parameter

const func = () => {
  console.log("hai");
};

// 2 can store a function in variables

const func1 = (print) => {
  print();
};

func1(func);

// 3 can return a function inside a function

const func2 = () => {
  const a = 10;
  return () => {
    console.log(a);
  };
};

const call = func2();
call();
