const n: number = 10;
const nn: number = "10";

const a: string = "hai";

const aa: any = { hai: "hai" };

function returnNumber(): number {
  return 7;
}

function returnString(): string {
  return "hai";
}

const arr: Array<number> = [1, 2, 3, 4, "q"];

function getNum(x: number) {
  console.log(x);
}

getNum(2);
