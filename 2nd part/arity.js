// arity is how may times we nesting the function with params thats called arity

function arity(e) {
  return (a) => {
    return (b) => {
      return (c) => {
        return (d) => {
          return a * b * c * d * e;
        };
      };
    };
  };
}
console.log(arity(1)(2)(3)(4)(5)); // arity of 5

function print(s) {
  return (str) => {
    return (string) => {
      console.log(s, str, string);
    };
  };
}

print("hii")("hello")("bye"); // arity of 3
