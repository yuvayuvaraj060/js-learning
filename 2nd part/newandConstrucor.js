// new key word

// new key word create new obj like blue print

function func(color, date) {
  (this.color = color),
    (this.date = date),
    (this.printAll = () => {
      console.log(this.color, this.date);
    });
}
const newFunc = new func("red", "20-2-05-2021");
const newFunc1 = new func("black", "20-2-05-2021");
console.log(newFunc);
console.log(newFunc1);
